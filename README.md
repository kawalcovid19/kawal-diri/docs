# Kawal Diri

**Part of [KawalCOVID19](https://kawalcovid19.quip.com/yYikAVfbUCsB/Product-SSOT-Kawal-COVID-19)**



## Overall enginering architecture (WIP):

https://app.cloudcraft.co/view/09b5f765-ac7f-4416-b586-71a84208abe7?key=AFTuXVIsRkq5AXEJ20vHGA

![image](https://user-images.githubusercontent.com/5461786/77437586-6bcb2500-6e17-11ea-8330-9b5dddaaf2ee.png)



## Repositories

#### - [APIRouter](https://gitlab.com/kawalcovid19/kawal-diri/api-router)

#### - [AuthService](https://gitlab.com/kawalcovid19/kawal-diri/authentication)

#### - [radar.io -> CosmosDB pipeline](https://gitlab.com/kawalcovid19/kawal-diri/tracker)

